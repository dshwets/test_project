README
=====================

This README describes all the steps required to create and run a web application.


### Docker settings

##### Installation

* [Detailed Installation Guide](https://docs.docker.com/install/linux/docker-ce/ubuntu/)

##### Commands to run docker without sudo (local)

* `sudo groupadd docker`
* `sudo gpasswd -a ${USER} docker`
* `newgrp docker`
* `sudo service docker restart`

##### docker health check without sudo

* `docker run hello-world`

### Docker-compose settings

##### Installation

* [Detailed Installation Guide](https://docs.docker.com/compose/install/)

##### Command to run docker-compose without sudo (local)

* `sudo ln -s /usr/local/bin/docker-compose /usr/bin/docker-compose`


### Local development

##### Commands for the first run

* `docker-compose build` - building docker containers
* `docker-compose up -d server` - run django application
* ` docker-compose exec server python ./manage.py migrate` - apply migrations
* ` docker-compose exec server python ./manage.py createsuperuser` - create superuser


##### Access

* http://localhost:8000 - start page
* http://localhost:8000/admin - Django admin panel
* http://localhost:8000/api/v1/auth/sign-up/ - sign-up
* http://localhost:8000/api/v1/auth/login/ - login
* http://localhost:8000/swagger/ - api-doc

* POST http://localhost:8000/api/v1/posts/ - create post (example body - {"title":"asd", "text":"ddas"})
* POST http://localhost:8000/api/v1/posts/like/ - like post (example body - {"post":1})
* PATCH http://localhost:8000/api/v1/posts/unlike/ - unlike post (example body - {"post":1})
* http://localhost:8000/api/v1/analytics/?date_from=2020-02-02&date_to=2022-03-14 - analitics by user likes or by all users if staff send request.
* http://localhost:8000/api/v1/auth/me - user activity

### Check_bot
#### Commands to run check_bot
! Django application must be configured first!

If u want change configuration of bot number_of_users,max_posts_per_user,max_likes_per_user
change values in test_file.json in check_bot folder.
* `docker-compose up check_bot` - run bot script
