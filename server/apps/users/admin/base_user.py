from django.contrib import admin
from apps.users.models import BaseUser


@admin.register(BaseUser)
class UserAdmin(admin.ModelAdmin):
    pass
