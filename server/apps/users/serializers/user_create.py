from django.contrib.auth.password_validation import validate_password
from django.core.exceptions import ValidationError
from rest_framework import serializers

from ..models import BaseUser


class UserCreateSerializer(serializers.ModelSerializer):
    password = serializers.CharField(write_only=True)

    class Meta:
        model = BaseUser
        fields = ('username', 'password')

    def create(self, validated_data: dict) -> BaseUser:
        try:
            user = BaseUser(**validated_data)
            validate_password(password=validated_data['password'], user=user)
        except ValidationError as e:
            raise serializers.ValidationError({'password': list(e)})

        user = BaseUser.objects.create_user(**validated_data)

        return user


class UserInfoSerializer(serializers.ModelSerializer):
    class Meta:
        model = BaseUser
        fields = ('username', 'last_login', 'last_request')
