from django.db import models
from django.contrib.auth.models import AbstractUser


class BaseUser(AbstractUser):
    last_request = models.DateTimeField(
        verbose_name='Last request',
        null=True
    )

    class Meta:
        verbose_name = 'User'
        verbose_name_plural = 'Users'
