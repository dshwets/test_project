from rest_framework import generics
from rest_framework.permissions import IsAuthenticated

from apps.users.models import BaseUser
from apps.users.serializers import UserCreateSerializer, UserInfoSerializer


class UserCreateAPIView(generics.CreateAPIView):
    serializer_class = UserCreateSerializer


class UserInfo(generics.RetrieveAPIView):
    serializer_class = UserInfoSerializer
    permission_classes = (IsAuthenticated,)

    def get_object(self) -> BaseUser:
        return self.request.user
