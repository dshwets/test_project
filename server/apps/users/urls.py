from django.urls import path
from rest_framework_simplejwt.views import TokenRefreshView, TokenObtainPairView

from .views import UserCreateAPIView, UserInfo

urlpatterns = [
    path('login/', TokenObtainPairView.as_view(), name='token_login',),
    path('me/', UserInfo.as_view(), name='user_info'),
    path('token-refresh/', TokenRefreshView.as_view(), name='token_refresh',),
    path('sign-up/', UserCreateAPIView.as_view(), name='user_signup',),
]
