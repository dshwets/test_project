from rest_framework import serializers

from ..models import Like


class BasePostLikeSerializer(serializers.ModelSerializer):
    user = serializers.CharField(read_only=True, )

    class Meta:
        model = Like
        fields = ('id', 'post', 'user', 'created_at',)


class PostLikeCreateSerializer(BasePostLikeSerializer):
    def validate(self, attrs: dict) -> dict:
        data = super().validate(attrs)

        if Like.objects.filter(post=data['post'], user=self.context['request'].user):
            raise serializers.ValidationError('Post already liked by user')

        return data

    def create(self, validated_data: dict) -> Like:
        post = Like.objects.create(
            user=self.context['request'].user,
            **validated_data,
        )

        return post
