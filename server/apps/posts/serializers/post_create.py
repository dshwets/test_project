from rest_framework import serializers

from ..models import Post


class PostCreateSerializer(serializers.ModelSerializer):
    author = serializers.CharField(read_only=True,)

    class Meta:
        model = Post
        fields = ('id', 'author', 'title', 'text')

    def create(self, validated_data: dict) -> Post:
        post = Post.objects.create(
            author=self.context['request'].user,
            **validated_data,
        )

        return post
