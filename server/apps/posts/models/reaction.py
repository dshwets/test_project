from django.db import models


class Like(models.Model):
    post = models.ForeignKey(
        to='posts.Post',
        on_delete=models.CASCADE,
        related_name='likes',
        verbose_name='post',
    )
    user = models.ForeignKey(
        to='users.BaseUser',
        on_delete=models.CASCADE,
        related_name='likes',
        verbose_name='user',
    )
    created_at = models.DateTimeField(
        auto_now_add=True,
        verbose_name='created_at',
    )

    class Meta:
        verbose_name = 'Like'
        verbose_name_plural = 'Likes'
