from django.db import models


class Post(models.Model):
    author = models.ForeignKey(
        to='users.BaseUser',
        on_delete=models.CASCADE,
        related_name='posts',
        verbose_name='author',
    )
    title = models.CharField(
        max_length=128,
        verbose_name='title'
    )
    text = models.TextField(
        max_length=4000,
        verbose_name='text',
    )

    class Meta:
        verbose_name = 'Post'
        verbose_name_plural = 'Posts'
