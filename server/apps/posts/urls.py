from django.urls import path

from .views import PostCreateAPIView, PostLikeCreateDestroyAPIView

urlpatterns = [
    path('', PostCreateAPIView.as_view(), name='user_signup',),
    path('like/', PostLikeCreateDestroyAPIView.as_view(), name='like'),
]
