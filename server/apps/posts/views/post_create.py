from rest_framework import generics
from rest_framework.permissions import IsAuthenticated

from apps.posts.serializers import PostCreateSerializer


class PostCreateAPIView(generics.CreateAPIView):
    serializer_class = PostCreateSerializer
    permission_classes = (IsAuthenticated,)
