from django.shortcuts import get_object_or_404
from rest_framework import generics, mixins
from rest_framework.permissions import IsAuthenticated

from apps.posts.models import Like
from apps.posts.serializers import PostLikeCreateSerializer


class PostLikeCreateDestroyAPIView(
    mixins.CreateModelMixin,
    mixins.DestroyModelMixin,
    generics.GenericAPIView
):
    serializer_class = PostLikeCreateSerializer
    permission_classes = (IsAuthenticated,)
    queryset = Like.objects.all()

    def get_object(self) -> Like:
        post_id = self.request.data.get('post')
        obj = get_object_or_404(
            self.queryset, post=post_id, user=self.request.user
        )
        self.check_object_permissions(self.request, obj)
        return obj

    def post(self, request, *args, **kwargs):
        return self.create(request, *args, **kwargs)

    def delete(self, request, *args, **kwargs):
        return self.destroy(request, *args, **kwargs)
