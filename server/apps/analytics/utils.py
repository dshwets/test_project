from datetime import datetime

from django.db.models import Count, QuerySet
from django.db.models.functions import TruncDate

from apps.posts.models import Like


class LikeAnalyticsService:
    @classmethod
    def get_queryset(
            cls, date_from: str, date_to: str, user_pk: int, is_staff=False,
    ) -> QuerySet:
        date_from = datetime.strptime(date_from, '%Y-%m-%d').date()
        date_to = datetime.strptime(date_to, '%Y-%m-%d').date()
        filters = {
            'created_at__date__range': [date_from, date_to]
        }

        if not is_staff:
            filters['user'] = user_pk

        return Like.objects.filter(
                **filters
            )

    @classmethod
    def get_likes_grouped_by_date(
            cls, date_from: str, date_to: str, user_pk: int, is_staff=False,
    ) -> dict:

        likes_count = list(
            cls.get_queryset(
                date_from=date_from,
                date_to=date_to,
                user_pk=user_pk,
                is_staff=is_staff
            ).annotate(
                date=TruncDate('created_at'),
            ).values(
                'date',
            ).annotate(
                count=Count('id'),
            ).order_by('date'),
        )
        likes_count_grouped_by_date = {
            count['date'].isoformat(): count['count'] for count in likes_count
        }
        return likes_count_grouped_by_date
