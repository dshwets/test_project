from rest_framework import status
from rest_framework.permissions import IsAuthenticated
from rest_framework.request import Request
from rest_framework.response import Response
from rest_framework.views import APIView
from drf_yasg import openapi
from drf_yasg.utils import swagger_auto_schema

from apps.analytics.serializers import DateValidationSerializer
from apps.analytics.utils import LikeAnalyticsService

date_from_param = openapi.Parameter(
    'date_from', openapi.IN_QUERY, description='start_date', type=openapi.FORMAT_DATE
)
date_to_param = openapi.Parameter(
    'date_to', openapi.IN_QUERY, description='finish_date', type=openapi.FORMAT_DATE
)


class LikeAnalyticsAPIView(APIView):
    permission_classes = (IsAuthenticated,)

    @swagger_auto_schema(manual_parameters=[date_from_param, date_to_param])
    def get(self, request: Request) -> Response:
        serializer = DateValidationSerializer(data=self.request.query_params)
        serializer.is_valid(raise_exception=True)

        return Response(
            LikeAnalyticsService.get_likes_grouped_by_date(
                user_pk=self.request.user.pk,
                is_staff=self.request.user.is_staff,
                **serializer.data
            ),
            status=status.HTTP_200_OK,
        )
