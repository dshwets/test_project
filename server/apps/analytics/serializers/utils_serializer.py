from typing import Final

from rest_framework import serializers


class DateValidationSerializer(serializers.Serializer):
    class Errors:
        INCORRECT_PARAMS: Final[str] = 'Incorrect params date_from and date_to.'

    date_from = serializers.DateField(required=True)
    date_to = serializers.DateField(required=True)

    def validate(self, attrs: dict) -> dict:
        date_from = attrs.get('date_from')
        date_to = attrs.get('date_to')
        if date_from > date_to:
            raise serializers.ValidationError(self.Errors.INCORRECT_PARAMS)
        return attrs
