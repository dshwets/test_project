from django.urls import include, path


urlpatterns = [
    path('auth/', include('apps.users.urls')),
    path('posts/', include('apps.posts.urls')),
    path('analytics/', include('apps.analytics.urls'))
]
