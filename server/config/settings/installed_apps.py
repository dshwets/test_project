INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'rest_framework',
    'drf_yasg',
]

LOCAL_APPS = [
    'apps.users',
    'apps.posts',
    'apps.analytics',
]

INSTALLED_APPS += LOCAL_APPS
