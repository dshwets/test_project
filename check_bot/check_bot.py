import json
import random
import string

import requests
from requests import HTTPError


class CheckBot:
    BASE_URL = 'http://server:8000/api/v1/'
    SIGN_UP_URL = BASE_URL + 'auth/sign-up/'
    LOGIN_URL = BASE_URL + 'auth/login/'
    POST_CREATE_URL = BASE_URL + 'posts/'
    POST_LIKE_URL = POST_CREATE_URL + 'like/'

    USERNAME_LENGTH = 20
    PASSWORD_LENGTH = 15
    POST_TITLE_LENGTH = 30
    POST_TEXT_LENGTH = 100

    @staticmethod
    def _get_base_character() -> list:
        return list(string.ascii_letters + string.digits)

    @staticmethod
    def _get_random_string_from_chararacters(
            characters: list, string_length: int,
    ) -> str:
        characters_copy = characters.copy()
        random.shuffle(characters_copy)
        characters_in_string = []
        for i in range(string_length):
            characters_in_string.append(random.choice(characters_copy))
        return ''.join(characters_in_string)

    @staticmethod
    def send_post_request_with_token(url: str, data: dict, token: str) -> requests.Response:
        response = requests.post(
            url=url,
            data=data,
            headers={'Authorization': f'Bearer {token}'}
        )
        response.raise_for_status()
        return response

    def _sign_up_user(self, username: str, password: str) -> requests.Response:
        response = requests.post(
            url=self.SIGN_UP_URL,
            data={
                'username': username,
                'password': password,
            },
        )
        return response

    def _generate_user_credentials(self) -> dict:
        characters = self._get_base_character()
        username = self._get_random_string_from_chararacters(
            characters=characters, string_length=self.USERNAME_LENGTH
        )
        characters += list('!@#$%^&*()')
        password = self._get_random_string_from_chararacters(
            characters=characters, string_length=self.PASSWORD_LENGTH
        )
        return {'username': username, 'password': password, }

    def _create_post(self, title: str, text: str, token: str) -> requests.Response:
        return self.send_post_request_with_token(
            url=self.POST_CREATE_URL,
            data={'title': title, 'text': text},
            token=token,
        )

    def _user_create_post(self, token: str) -> requests.Response:
        characters = self._get_base_character()
        post_title = self._get_random_string_from_chararacters(
            characters=characters, string_length=self.POST_TITLE_LENGTH,
        )
        post_text = self._get_random_string_from_chararacters(
            characters=characters, string_length=self.POST_TEXT_LENGTH,
        )
        return self._create_post(title=post_title, text=post_text, token=token)

    def _login_user(self, username: str, password: str) -> requests.Response:
        response = requests.post(
            url=self.LOGIN_URL,
            data={'username': username, 'password': password},
        )
        return response

    def _get_users_tokens(self):
        self.access_tokens = []
        for credential in self.users_credentials:
            response = self._login_user(**credential)
            self.access_tokens.append(response.json()['access'])

    def _like_post(self, post_id: int, token: str) -> None:
        try:
            self.send_post_request_with_token(
                url=self.POST_LIKE_URL,
                data={'post': post_id},
                token=token,
            )
        except HTTPError as e:
            print(f'Duplicated like.  Error: {e.response.json()}')

    def _like_random_post(self, token: str) -> None:
        random_post_id = random.choice(self.posts_id)
        self._like_post(post_id=random_post_id, token=token)

    def _users_like_posts(self) -> None:
        for aceess_token in self.access_tokens:
            for i in range(
                    random.randrange(
                        0,
                        self.config.get('max_likes_per_user', 1) + 1,
                    )
            ):
                if self.posts_id:
                    self._like_random_post(token=aceess_token)

    def _sign_up_users_and_write_tokens(self) -> None:
        count_of_users = self.config.get('number_of_users', 0)
        self.users_credentials = []
        signed_up_users = 0
        while signed_up_users < count_of_users:
            credentials = self._generate_user_credentials()
            response = self._sign_up_user(**credentials)
            if response.status_code == 201:
                self.users_credentials.append(credentials)
                signed_up_users += 1
        self._get_users_tokens()

    def _users_create_posts(self) -> None:
        self.posts_id = []
        for aceess_token in self.access_tokens:
            for i in range(
                    random.randrange(
                        0,
                        self.config.get('max_posts_per_user', 1) + 1,
                    )
            ):
                response = self._user_create_post(aceess_token)
                self.posts_id.append(response.json()['id'])

    def _read_config_from_file(self) -> None:
        with open('test_file.json') as file:
            self.config = json.load(file)

    @classmethod
    def run_bot(cls) -> None:
        bot = cls()
        bot._read_config_from_file()
        bot._sign_up_users_and_write_tokens()
        bot._users_create_posts()
        bot._users_like_posts()


if __name__ == '__main__':
    CheckBot.run_bot()
